import pytest
import time



if __name__ == "__main__":
    #TODO:
    # 1. purge old runtime json files if fresh installation
    # 2. purge old logs and XMLs if fresh installation
    junit_path = '.\Reports\/Junit_' + time.strftime("%d%m%Y_%H%M%S") + '.xml'
    result_log_path = '.\Reports\/Logs\/Execution_' + time.strftime("%d%m%Y_%H%M%S") + '.log'
    pytest.main(['-v', '-x', '--junitxml', junit_path, '--resultlog', result_log_path])
    pass
