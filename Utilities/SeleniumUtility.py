from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import WebDriverException
import selenium.common.exceptions


class SeleniumUtils(object):
    """
     This class holds all selenium related actions in order to simplify invocation and maintenance
    """

    def __init__(self, browser_type='Chrome', hub_start=False):
        # print('---> SeleniumUtility init')
        if browser_type == 'Chrome':
            try:
                # pass
                self.driver = webdriver.Chrome()
            except WebDriverException:
                print WebDriverException.message
                raise WebDriverException
                print('SeleniumUtility.init : Driver initialization failed for : ' + browser_type)
            if hub_start:
                self.driver = webdriver.Remote(command_executor='http://192.168.0.87:4444/wd/hub',
                                               desired_capabilities=DesiredCapabilities.CHROME)
        elif browser_type == 'Firefox':
            try:
                firefox_capabilities = DesiredCapabilities().FIREFOX
                firefox_capabilities["marionette"] = True
                self.driver = webdriver.Firefox(capabilities=firefox_capabilities)
            except WebDriverException:
                raise WebDriverException
                print('SeleniumUtility.init : Driver initialization failed for : ' + browser_type)
            if hub_start:
                self.driver = webdriver.Remote(command_executor='http://192.168.0.87:4444/wd/hub',
                                               desired_capabilities=DesiredCapabilities.FIREFOX)
        elif browser_type == 'IE':
            cap = webdriver.DesiredCapabilities.INTERNETEXPLORER
            cap['ignoreProtectedModeSettings'] = True
            cap['IntroduceInstabilityByIgnoringProtectedModeSettings'] = True
            cap['nativeEvents'] = True
            cap['ignoreZoomSetting'] = True
            cap['requireWindowFocus'] = True
            cap['INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS'] = True
            cap['unexpectedAlertBehaviour'] = 'accept'
            cap['disable-popup-blocking'] = True
            cap['enablePersistentHover'] = True
            cap['javaScriptEnabled'] = True
            cap['ie.ensureCleanSession'] = True
            try:
                self.driver = webdriver.Ie(capabilities=cap)
            except WebDriverException:
                raise WebDriverException
                print('SeleniumUtility.init : Driver initialization failed for : ' + browser_type)
            if hub_start:
                self.driver = webdriver.Remote(command_executor='http://192.168.0.87:4444/wd/hub',
                                               desired_capabilities=DesiredCapabilities.INTERNETEXPLORER)
        else:
            raise NotImplementedError
            print('WebDriver init --> Failed: Not Supported browser type')

        # print('---> SeleniumUtility init --> Finished')

    def get_url(self, url, url_title):
        """
        Opens
        :param url:
        :param url_title:
        :return: -----
        """
        if len(url) == 0 or len(url_title) == 0:
            raise NotImplementedError
            print('Provided inputs are invalid')
        else:
            self.driver.get(url)
            self.driver.maximize_window()
        try:
            assert url_title in self.driver.title
        except WebDriverException:
            self.driver.save_screenshot("UrlValidationFailed.png")

    def assert_element(self, *args):
        try:
            args
        except NoSuchElementException:
            raise NoSuchElementException
            print('Web Element assertion : Failed!')

    def get_element(self, element_locators):
        """
        Retrieves a web element based on inputs
        :param **element_locators: a dictionary : key is locator type and value is locator
        :return: located_element
        """
        if len(element_locators) > 0:
            for key in element_locators:
                # retrieve locator_type from dictionary key
                # retrieve locator  from dictionary value
                locator_type = key
                locator = element_locators[key]

                if locator_type == "id":
                    try:
                        located_element = self.driver.find_element_by_id(locator)
                    except NoSuchElementException:
                        raise NoSuchElementException
                        print('Failed to element retrieve by:' + locator_type)
                elif locator_type == "xpath":
                    try:
                        located_element = self.driver.find_element_by_xpath(locator)
                    except NoSuchElementException:
                        raise NoSuchElementException
                        print('Failed to element retrieve by:' + locator_type)
                elif locator_type == "link text":
                    try:
                        located_element = self.driver.find_element_by_link_text(locator)
                    except NoSuchElementException:
                        raise NoSuchElementException
                        print('Failed to retrieve by:' + locator_type)
                elif locator_type == "partial link text":
                    try:
                        located_element = self.driver.find_element_by_partial_link_text(locator)
                    except NoSuchElementException:
                        raise NoSuchElementException
                        print('Failed to retrieve by:' + locator_type)
                elif locator_type == "name":
                    try:
                        located_element = self.driver.find_element_by_name(locator)
                    except NoSuchElementException:
                        raise NoSuchElementException
                        print('Failed to retrieve by:' + locator_type)
                elif locator_type == "tag":
                    try:
                        located_element = self.driver.find_element_by_tag_name(locator)
                    except NoSuchElementException:
                        raise NoSuchElementException
                        print('Failed to retrieve by:' + locator_type)
                elif locator_type == "class":
                    try:
                        located_element = self.driver.find_element_by_class_name(locator)
                    except NoSuchElementException:
                        raise NoSuchElementException
                        print('Failed to retrieve by:' + locator_type)
                elif locator_type == "css":
                    try:
                        located_element = self.driver.find_element_by_css_selector(locator)
                    except NoSuchElementException:
                        raise NoSuchElementException
                        print('Failed to retrieve by:' + locator_type)
                else:
                    raise NotImplementedError
                    print('This element location method is not supported yet')
        else:
            raise NotImplementedError
            print('This element location method is not supported yet')

        try:
            assert located_element
        except NoSuchElementException:
            self.driver.save_screenshot("ElementFailedToLocate.png")

        return located_element

    def get_elements(self, element_locators):
        """
                Retrieves a list of web elements based on inputs
                :param locator_type:
                :param locator:
                :return: located_element
                """
        if len(element_locators) > 0:
            for key in element_locators:
                # retrieve locator_type from dictionary key
                # retrieve locator  from dictionary value
                locator_type = key
                locator = element_locators[key]
                if locator_type == "id":
                    try:
                        elements_list = self.driver.find_elements_by_id(locator)
                    except NoSuchElementException:
                        raise NoSuchElementException
                elif locator_type == "xpath":
                    try:
                        elements_list = self.driver.find_elements_by_xpath(locator)
                    except NoSuchElementException:
                        raise NoSuchElementException
                elif locator_type == "link text":
                    try:
                        elements_list = self.driver.find_elements_by_link_text(locator)
                    except NoSuchElementException:
                        raise NoSuchElementException
                elif locator_type == "partial link text":
                    try:
                        elements_list = self.driver.find_elementsby_partial_link_text(locator)
                    except NoSuchElementException:
                        raise NoSuchElementException
                elif locator_type == "name":
                    try:
                        elements_list = self.driver.find_elements_by_name(locator)
                    except NoSuchElementException:
                        raise NoSuchElementException
                elif locator_type == "tag":
                    try:
                        elements_list = self.driver.find_elements_by_tag_name(locator)
                    except NoSuchElementException:
                        raise NoSuchElementException
                elif locator_type == "class":
                    try:
                        elements_list = self.driver.find_elements_by_class_name(locator)
                    except NoSuchElementException:
                        raise NoSuchElementException
                elif locator_type == "css":
                    try:
                        elements_list = self.driver.find_elements_by_class_name(locator)
                    except NoSuchElementException:
                        raise NoSuchElementException
                else:
                    raise NotImplementedError
                    print('This element location method is not supported yet')
        else:
            raise NotImplementedError
            print('This element location method is not supported yet')

        try:
            assert elements_list
        except NoSuchElementException:
            self.driver.save_screenshot("ElementsFailedToLocate.png")
        return elements_list

    def get_attribute(self, element, attribute):
        # TODO: check if works
        """
        This method retrieves the value of the attribute for example text value and so on.
        :param element:
        :param attribute:
        :return: the value of the attribute in the given element
        """
        attribute_value = element.get_attribute(attribute)

        return attribute_value

    def perform_action(self, action_type, element, input_value=None, validation_element=None): #validation_element : should be a dictionary
        """
        Performs an action on the designated element
        :param action_type: click or type
        :param input_value: if selected action type is type this should be the value to be entered
        :param element: element on which the action should be performed
        :param validation_element: element which will serve as a validation point for step success/failure indication
        :return: indicates success
        """
        if len(action_type) == 0:
            raise NotImplementedError
        elif action_type == 'Wait':
            # should be modified to handle wait period input
            self.driver.implicitly_wait(int(input_value))
        elif action_type == 'Click':
            try:
                element.click()
            except selenium.common.exceptions.ElementClickInterceptedException:
                emb_element = element[0]
                emb_element.click()
                raise selenium.common.exceptions.ElementClickInterceptedException
        elif action_type == 'Type':
            if len(input_value) != 0:
                element.send_keys(input_value)
            else:
                raise NotImplementedError
        elif action_type == 'SlowType':
            if len(input_value) != 0:
                for character in input_value:
                    element.send_keys(character)
                    time.sleep(0.3)  # pause for 0.3 seconds
            else:
                raise NotImplementedError
        elif action_type == 'ClearText':
                element.clear()
                print(element.get_attribute('value'))
                # if len(element.get_attribute('value')) < 1:
                #     raise WebDriverException
                #     print('Failed to clear Text from Field: ' + element)
        if validation_element:
            return self.get_element(validation_element)
        else:
            return True

    def reload_page(self):
        self.driver.refresh()
