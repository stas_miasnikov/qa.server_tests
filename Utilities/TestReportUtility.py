import requests
import json


class TestResultUtility:

    # TODO:
    # 1. Update authorization data to use own or automation credentials.
    # 2. Add implementation of class to main
    # 3. Add JUnit XML implementation to generate results --> Done in invocation

    def __init__(self):
        self.steps = []

    # steps are an array of maps
    def func_to_run_after_test(self, instance_id):
        data_json = json.dumps(
            {'data':
                {
                    'type': 'instances',
                    'attributes': {
                        'instance-id': + instance_id,
                    }, "steps": {"data": self.steps}}}
                )

        r = requests.post("https://api.practitest.com/api/v2/projects/3030/runs.json",
                          data=data_json,
                          auth=('shlomit.ahituv@morphisec.com', 'a759a50060228f2830253ded6db9e8b3142b3941'),
                          headers={'Content-type': 'application/json'})
        print r.status_code
        print r.text

    def set_steps(self, updated_steps):
        self.steps = updated_steps

    def update_step(self, step_num, status):
        self.steps[step_num]['status'] = status

    def add_to_steps(self, step_to_add):
        self.steps.append(step_to_add)

    def generate_junit_report(self):
        pass
