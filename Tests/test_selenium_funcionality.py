# config.py
from selenium.webdriver.common.action_chains import ActionChains
from Utilities.SeleniumUtility import SeleniumUtils
from selenium.common.exceptions import ElementNotVisibleException
from Utilities.SolrUtility import SolrUtility
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import time
import inspect
import pytest
import selenium.common.exceptions
import unittest
import yaml


class TestExecutor(object):

    @classmethod
    def setup_class(cls):
        """
        Sets up all required objects
        :return: N/A
        """
        # TODO: DONE
        cls.selenium_driver = SeleniumUtils()
        cls.solr_driver = SolrUtility()

    # Open Server UI
    def test_open_server_url(self, server_url=None):
        """
        Opens the server UI URL
        :param server_url - should be provided from input (ip/url)
        :return:  true/false based on assertion
        """
        # TODO: DONE
        if server_url is not None:
            self.selenium_driver.get_url(server_url, "Morphisec")
        else:
            self.selenium_driver.get_url("https://localhost/#/login", "Morphisec")
        print(inspect.stack()[0][3] + ": Finished \n")
        pass

    def test_ui_verify_login_screen(self):
        """
        Verifies all Login Screen have loaded correctly and visible
        :return: assertion
        """
        # TODO: DONE
        # Verify UI is open
        assert "Morphisec" in self.selenium_driver.driver.title

        login_elements = {"id": "username", "xpath": "//*[contains(text(), 'Login to your account')]", "id": "password",
                          "xpath": "//*[contains(text(), 'LOGIN')]",
                          "xpath": "//img[@src='/assets/images/common/user_login_icon.png']",
                          "xpath": "//img[@src='/assets/images/login/password_icon.png']",
                          "class": "forgot-password-button"}
        assert self.selenium_driver.get_element(login_elements)
        print(inspect.stack()[0][3] + ": Finished \n")
        pass

    def test_login_negative_no_password(self):
        """
        This is a negative test for the login screen
        :return: PASS if the login button is disabled
        """

        self.selenium_driver.driver.refresh()
        # TODO: DONE
        # Verify UI is open
        assert "Morphisec" in self.selenium_driver.driver.title

        # Enter username:
        user_name = {"id": "username"}
        self.selenium_driver.perform_action('Type', self.selenium_driver.get_element(user_name), 'morphisec')
        username_text = self.selenium_driver.get_attribute(self.selenium_driver.get_element(user_name), 'value')
        assert username_text == 'morphisec'

        # Verify Login Button state is disabled i.e. enabled() == false
        login_button = {"xpath": "//*[contains(text(), 'LOGIN')]"}
        button_state = self.selenium_driver.get_element(login_button)
        assert button_state.is_enabled() is False
        print(inspect.stack()[0][3] + ": Finished \n")
        pass

    def test_login_negative_no_username(self):
        """
        Negative Test to see that the login button is disabled if only the password is entered
        :return:  PASS if the button is disabled.
        """
        # TODO: DONE
        self.selenium_driver.driver.refresh()
        # Verify UI is open
        assert "Morphisec" in self.selenium_driver.driver.title

        # Set password only
        password = {"id": "password"}
        self.selenium_driver.perform_action('ClearText', self.selenium_driver.get_element(password))
        self.selenium_driver.perform_action('Type', self.selenium_driver.get_element(password), '1q2w#E$R')
        password_text = self.selenium_driver.get_attribute(self.selenium_driver.get_element(password), 'value')
        assert len(password_text) > 0

        # Verify Login Button state is disabled i.e. enabled() == false
        login_button = {"xpath": "//*[contains(text(), 'LOGIN')]"}
        button_state = self.selenium_driver.get_element(login_button)
        assert button_state.is_enabled() is False  # ---> disabled as currently it is failing
        print(inspect.stack()[0][3] + ": Finished \n")
        pass

    def test_login_positive(self):
        """
        Positive Test : login in to the system
        :return: logged in and verificatio nthat a key element is visible
        """
        # TODO: DONE
        # Verify UI is open
        assert "Morphisec" in self.selenium_driver.driver.title

        # Clear all Fields
        self.selenium_driver.driver.refresh()

        time.sleep(5)
        # Enter username:
        user_name = {"id": "username"}
        self.selenium_driver.perform_action('Type', self.selenium_driver.get_element(user_name), 'morphisec')
        username_text = self.selenium_driver.get_attribute(self.selenium_driver.get_element(user_name), 'value')
        assert username_text == 'morphisec'

        # Verify Login Button state is disabled i.e. enabled() == false
        login_button = {"xpath": "//*[contains(text(), 'LOGIN')]"}
        button_state = self.selenium_driver.get_element(login_button)
        assert button_state.is_enabled() is False

        # Enter password
        password = {"id": "password"}
        self.selenium_driver.perform_action('Type', self.selenium_driver.get_element(password), '1q2w#E$R')
        password_text = self.selenium_driver.get_attribute(self.selenium_driver.get_element(password), 'value')
        assert len(password_text) > 0

        # Verify Login Button state is enabled
        button_state = self.selenium_driver.get_element(login_button)
        assert button_state.is_enabled() is True
        # if assert passes click on the button
        self.selenium_driver.perform_action('Click', self.selenium_driver.get_element(login_button))
        print(inspect.stack()[0][3] + ": Finished \n")
        pass

    # Dashboard view tests area

    def test_verify_server_dashboard_is_loaded(self):
        """
        Verification test to identify that current system is displayed
        :return:
        """
        # TODO: DONE
        # Setting the browser in focus on the opened UI page:
        verification_element = {"class": "layout"}
        self.selenium_driver.get_element(verification_element)
        verification_element = {"class": "title"}
        time.sleep(1)
        dashboard_title = self.selenium_driver.get_element(verification_element)
        assert dashboard_title is not None

        # Verify screen is open
        value = dashboard_title.get_attribute('innerHTML')
        assert value == 'Dashboard'

        # Verify timestamp is matching
        verification_timestamp = {"class": "topbar-time"}
        timestamp_element = self.selenium_driver.get_element(verification_timestamp)
        time_stamp = self.selenium_driver.get_attribute(timestamp_element, 'innerHTML')
        assert time_stamp == time.strftime("%#d %b %y %#I:%M %p")
        print(inspect.stack()[0][3] + ": Finished \n")
        pass

    def test_verify_dashboard_ui_protectors_components(self):
        """
        Verifying protector components are visible and represting correct values (counters)
        :return:
        """
        # TODO: NOT DONE
        # Add input values to verify expected number of protectors and plans are displayed
        # Currently using hard coded values.

        # Protectors verification
        # Setting the Protectors container in context
        verification_timestamp = {"class": "topbar-time"}
        self.selenium_driver.get_element(verification_timestamp)

        protector_widget = {"xpath": "//*[contains(text(), 'NOT PROTECTING')]"}
        assert self.selenium_driver.get_element(protector_widget)
        # Get not protecting widget
        not_protected = {"id": "qa-widgetNotProtecting"}
        not_protected_widget = self.selenium_driver.get_elements(not_protected)
        try:
            counter = not_protected_widget[0].find_element_by_tag_name("span")
        except ValueError:
            raise ValueError
        # Assert protecting widget count
        # TODO: add input variable in order to avoid use of hardcoded values
        # Not running protectors
        assert int(self.selenium_driver.get_attribute(counter, 'innerHTML')) >= 0

        protector_widget = {"xpath": "//*[contains(text(), 'UNKNOWN')]"}
        assert self.selenium_driver.get_element(protector_widget)
        # TODO : add verification for number of protectors
        # Get Unknown widget
        uknown_protectors = {"id": "qa-widgetUnknown"}
        uknown_widget = self.selenium_driver.get_elements(uknown_protectors)
        try:
            counter = uknown_widget[0].find_element_by_tag_name("span")
        except ValueError:
            raise ValueError
        # Assert protecting widget count
        # TODO: add input variable in order to avoid use of hardcoded values
        # Unknown protectors
        assert int(self.selenium_driver.get_attribute(counter, 'innerHTML')) >= 0

        protector_widget = {"xpath": "//*[contains(text(), 'RUNNING')]"}
        assert self.selenium_driver.get_element(protector_widget)
        # Get Running widget
        running_protectors = {"id": "qa-widgetRunning"}
        running_widget = self.selenium_driver.get_elements(running_protectors)
        try:
            counter = running_widget[0].find_element_by_tag_name("span")
        except ValueError:
            raise ValueError
        # TODO: add input variable in order to avoid use of hardcoded values
        # Running protectors
        assert int(self.selenium_driver.get_attribute(counter, 'innerHTML')) >= 0

        # Troubleshooting plans
        troubleshooting_plans = {"class": "troubleshoot-plans"}
        assert self.selenium_driver.get_element(troubleshooting_plans)
        assert self.selenium_driver.get_attribute(self.selenium_driver.get_element(troubleshooting_plans),
                                                  'innerHTML') == '0 Troubleshooting Plans'
        # Troubleshooting protectors
        troubleshooting_protectors = {"class": "protectors"}
        assert self.selenium_driver.get_element(troubleshooting_protectors)
        assert self.selenium_driver.get_attribute(self.selenium_driver.get_element(troubleshooting_protectors),
                                                  'innerHTML') == '0 Protectors'

        print(inspect.stack()[0][3] + ": Finished \n")
        pass

    # Dashboard Attacks tests area
    def test_verify_dashboard_ui_attacks(self):
        """
        Verifies correct information is displayed in the attacks widget on the dashboard screen
        :return:
        """

        #TODO - NOT DONE
        # Add Total attacks count not hardcoded
        # Add different time window verification

        attacks_widget = {"xpath": "//*[@class='attacks-widget attacks']"}

        total_attack_count = self.selenium_driver.get_elements(attacks_widget)[0].\
            find_element_by_class_name("dashboard-widget-header-count-text")
        # Get number of logged attacks: dashboard-widget-header-count-text
        total_attacks_attribute = self.selenium_driver.get_attribute(total_attack_count, "innerHTML")
        assert int(total_attacks_attribute) != 0  # TODO fix this part for the verification not to be hardcoded

        # Get top 5 attacked APPS
        list_of_apps = self.selenium_driver.get_elements(attacks_widget)[0]\
            .find_elements_by_class_name("dashboard-widget-footer-graph-list-item")
        self.get_top_5_data(list_of_apps)
        date_filter = {"xpath": "//*[@class='date-filter ng-star-inserted']"}
        self.selenium_driver.get_elements(date_filter)[0].click()
        dropdown = {"class": "ui-dropdown-items-wrapper"}
        assert self.selenium_driver.get_elements(dropdown)
        dates = self.selenium_driver.get_elements(dropdown)[0].find_elements_by_tag_name("li")
        top_range = len(dates)
        for i in range(0, top_range):
            dates[i].click()
            time.sleep(2)
            # TODO insert here getting top 5 APPs - DONE
            list_of_apps = self.selenium_driver.get_elements(attacks_widget)[0] \
                .find_elements_by_class_name("dashboard-widget-footer-graph-list-item")
            self.get_top_5_data(list_of_apps)
            date_filter = {"xpath": "//*[@class='date-filter ng-star-inserted']"}
            self.selenium_driver.get_elements(date_filter)[0].click()
            dropdown = {"class": "ui-dropdown-items-wrapper"}
            dates = self.selenium_driver.get_elements(dropdown)[0].find_elements_by_tag_name("li")
            time.sleep(2)
        time.sleep(2)
        print(inspect.stack()[0][3] + ": Finished \n")
        pass

    def test_verify_dashboard_ui_protected_apps(self):
        # pass
        # Verifies protected apps are visible
        # Step one is setting focus on the component
        widget_wrapper = {"class": "attacks-widgets-wrapper"}
        self.selenium_driver.get_element(widget_wrapper).click()
        protected_widget_xpath = {"xpath": "//*[@class='attacks-widget protected-apps']"}
        self.selenium_driver.get_elements(protected_widget_xpath)

        total_app_count = self.selenium_driver.get_elements(protected_widget_xpath)[0]. \
            find_element_by_class_name("dashboard-widget-header-count-text")
        # Get number of logged attacks: dashboard-widget-header-count-text
        total_protected_apps_attribute = self.selenium_driver.get_attribute(total_app_count, "innerHTML")
        assert int(total_protected_apps_attribute) > 0  # TODO fix this part for the verification not to be hardcoded

        # Get top 5 protected APPS
        list_of_apps = self.selenium_driver.get_elements(protected_widget_xpath)[0] \
            .find_elements_by_class_name("dashboard-widget-footer-graph-list-item")
        self.get_top_5_data(list_of_apps)

        # cycle through time windows
        date_filter = {"xpath": "//*[@class='dashboard-widget-header-date-filter']"}
        self.selenium_driver.get_elements(date_filter)[1].click()
        dropdown = {"class": "ui-dropdown-items-wrapper"}
        assert self.selenium_driver.get_elements(dropdown)
        dates = self.selenium_driver.get_elements(dropdown)[1].find_elements_by_tag_name("li")
        top_range = len(dates)
        for i in range(0, top_range):
            dates[i].click()
            time.sleep(2)
            # TODO insert here getting top 5 Protected APPs - DONE
            list_of_apps = self.selenium_driver.get_elements(protected_widget_xpath)[0] \
                .find_elements_by_class_name("dashboard-widget-footer-graph-list-item")
            self.get_top_5_data(list_of_apps)
            date_filter = {"xpath": "//*[@class='date-filter ng-star-inserted']"}
            self.selenium_driver.get_elements(date_filter)[1].click()
            dropdown = {"class": "ui-dropdown-items-wrapper"}
            dates = self.selenium_driver.get_elements(dropdown)[1].find_elements_by_tag_name("li")
            time.sleep(2)
        time.sleep(2)

        print(inspect.stack()[0][3] + ": Finished \n")
        pass
    # End of Dashboard Screen validation

    # def test_attacks_screen(self):
    #     self.navigate_to_screen('ATTACKS')
    #     time.sleep(5)
    #     attack_distribution = {'class': 'top-attacks-entries'}
    #     assert self.selenium_driver.get_element(attack_distribution)
    #
    #     # top_bar = {"xpath": "//*[@class='topbar sidebar-open']"}
    #     # span_tags = self.selenium_driver.get_element(top_bar).find_element_by_tag_name('span')
    #     # attak_number_unformatted  = span_tags.get_attribute('innerHTML')
    #     # attack_number = str(attak_number_unformatted).translate(None, '()')  # TODO compare agasint expected value
    #     date_selector={"id": "qa-attackDateFilter"}
    #     self.selenium_driver.get_element(date_selector).click()
    #     range_box = {"class": "box-content"}
    #     date_range = self.selenium_driver.driver.find_element_by_class_name("date-filter-content").find_elements_by_tag_name("span")
    #     full_range = len(date_range)
    #     for i in range(0, full_range):
    #         date_range[i].click()
    #         # xx = date_range[i].get_attribute("innerHTML") #DEBUG PURPOSE
    #         if date_range[i].get_attribute("innerHTML") != 'Custom':
    #             time.sleep(2)
    #             # TODO: Get # of Attacks
    #             top_bar = {"xpath": "//*[@class='topbar sidebar-open']"}
    #             span_tags = self.selenium_driver.get_element(top_bar).find_element_by_tag_name('span')
    #             attak_number_unformatted = span_tags.get_attribute('innerHTML')
    #             attack_number = str(attak_number_unformatted).translate(None, '()')  # TODO compare agasint expected value
    #             # TODO get values of attack distribution widget
    #             assert self.selenium_driver.get_element(attack_distribution)
    #             attacked_counters = {"class": "top-attacks-count"}
    #             assert self.selenium_driver.get_elements(attacked_counters)
    #             list_of_counters = self.selenium_driver.get_elements(attacked_counters)
    #             for counter in list_of_counters:
    #                 counter.click()
    #             attacked_labels = {"class": "top-attacks-label"}
    #             assert self.selenium_driver.get_elements(attacked_labels)
    #
    #             # TODO click on Open attacks list window
    #             if int(attack_number) > 0:
    #                 self.navigate_to_attacks_list()
    #                 time.sleep(2)
    #
    #             # TODO click back
    #
    #         elif date_range[i].get_attribute("innerHTML") == 'Custom':
    #             print('not yet implemented')  # TODO replace with date inputs
    #             # TODO: Get # of Attacks
    #             top_bar = {"xpath": "//*[@class='topbar sidebar-open']"}
    #             span_tags = self.selenium_driver.get_element(top_bar).find_element_by_tag_name('span')
    #             attak_number_unformatted = span_tags.get_attribute('innerHTML')
    #             attack_number = str(attak_number_unformatted).translate(None,
    #                                                                     '()')  # TODO compare agasint expected value
    #             # TODO get values of attack distribution widget
    #             assert self.selenium_driver.get_element(attack_distribution)
    #             # TODO click on Open attacks list window
    #             pass
    #
    #         # TODO: switch to next dropdown
    #         self.selenium_driver.get_element(date_selector).click()
    #         time.sleep(2)
    #
    #     print(inspect.stack()[0][3] + ": Finished \n")
    #     pass

    def navigate_to_attacks_list(self):
        open_button = {"id": "qa-openAttacksListButton"}
        assert self.selenium_driver.get_element(open_button)
        self.selenium_driver.get_element(open_button).click()
        # TODO perform list validation and check attack details
        attack_log_entry = {"class": "datatable-body"}
        x = self.selenium_driver.get_elements(attack_log_entry)
        single_attack = {"xpath": "//*[contains(text(), 'Show Log')]"}
        # single_attack = {"class": "datatable-body-cell-label"}
        time.sleep(2)
        log_entries = self.selenium_driver.get_elements(single_attack)
        assert log_entries
        # assert len(log_entries) >= 0 # TODO compare vs expected
        for log in log_entries:
            log.click()
            attack_json = {"class": "json-viewer"}
            assert self.selenium_driver.get_element(attack_json)
            json_value = self.selenium_driver.get_element(attack_json).text # TODO do someting with the JSON Value
            copy_button = {"xpath": "//*[contains(text(), 'Copy')]"}
            assert self.selenium_driver.get_element(copy_button)
            export_button = {"xpath": "//*[contains(text(), 'Export')]"}
            assert self.selenium_driver.get_element(export_button)
            close_button = {"xpath": "//*[@href='#']"}
            # TODO close the open window
            # assert self.selenium_driver.get_element(close_button)
            # assert self.selenium_driver.get_element(close_button).find_element_by_tag_name("span")
            # self.selenium_driver.get_element(close_button).find_element_by_tag_name("span").click()
            # self.selenium_driver.driver.send_keys(Keys.ESCAPE)
            pass


        time.sleep(2)

        # TODO go back to Attack screen
        back_arrow = {"class": "topbar-back-text"}
        assert self.selenium_driver.get_element(back_arrow)
        self.selenium_driver.get_element(back_arrow).click()

        pass

    def test_protectors_screen(self):
        self.navigate_to_screen('PROTECTORS')

        protector_tuple = {"xpath": "//*[contains(@class,'version-column')]"}
        assert self.selenium_driver.driver.find_elements_by_xpath("//*[contains(@class,'version-column')]")
        protectors_list = self.selenium_driver.get_elements(protector_tuple)

        protectors_list = self.selenium_driver.driver.find_elements_by_tag_name("datatable-scroller")
        # for protector in protectors_list:




        print(inspect.stack()[0][3] + ": Finished \n")
        pass

    # def test_plans_screen(self):
    #     test_case = 'Verify Test Plans Screens'
    #     self.navigate_to_screen('PLANS')
    #     print(inspect.stack()[0][3] + ": Finished \n")
    #     pass
    #
    # def test_general_settings_tab(self):
    #     self.navigate_to_screen('SETTINGS')
    #
    #     # Select tab bar
    #     tab_bar = {"xpath": "//*[@class='ui-tabview-nav ui-helper-reset ui-helper-clearfix ui-widget-header "
    #                         "ui-corner-all ng-star-inserted']"}
    #     settings_tabbar = self.selenium_driver.get_elements(tab_bar)
    #     # self.check_unsaved_changes_popup()
    #
    #     # General Settings:
    #     server_domain_label = {"xpath": "//*[contains(text(), 'Server Domain Name')]"}
    #     assert self.selenium_driver.get_element(server_domain_label)
    #     server_name = {"name": "domainUrl"}
    #     server_domain_name = self.selenium_driver.get_element(server_name)
    #     server_domain_name.clear()
    #     time.sleep(5)
    #
    #     self.selenium_driver.perform_action('SlowType', server_domain_name, 'hello')
    #     # assert self.selenium_driver.get_attribute(text_field, 'value') == 'hello'
    #
    #     server_domain_tooltip = {"xpath": "//img[@src='/assets/images/common/user_login_icon.png']"}
    #     assert self.selenium_driver.get_element(server_domain_tooltip)
    #     # hover_tooltip = ActionChains(self.selenium_driver).move_to_element(self.selenium_driver.get_element(server_domain_tooltip))
    #     # hover_tooltip.perform()
    #     allow_uninstall_checkbox = {"xpath": "//*[contains(text(), 'Allow uninstalling protectors')]"}
    #     assert self.selenium_driver.get_element(allow_uninstall_checkbox)
    #     # x = self.selenium_driver.get_element(allow_uninstall_checkbox).is_selected()
    #     self.selenium_driver.perform_action('Click', self.selenium_driver.get_element(allow_uninstall_checkbox))
    #     # verify allow uninstalling protectors is enabled
    #     # assert self.selenium_driver.get_element(allow_uninstall_checkbox).is_selected()
    #     date_dropdown = {"id": "qa-dateDropDownOptions"}
    #     assert self.selenium_driver.get_element(date_dropdown)
    #     # Open Log Date drop down
    #     self.selenium_driver.get_element(date_dropdown).click()
    #     x = self.selenium_driver.get_element(date_dropdown)
    #     dropdown_values = x.find_elements_by_class_name("ui-dropdown-items-wrapper")
    #     report_months_list = []
    #     # print (dropdown_values[0].text)
    #     report_months_list = str(dropdown_values[0].text).split("\n")
    #     # print report_months_list  # debugging purposes
    #     for month in report_months_list:
    #         dropdown_tuple = self.selenium_driver.driver.find_element_by_xpath("//*[contains(text(), '" + month + "')]")
    #         assert dropdown_tuple
    #         # dropdown_tuple.click()
    #         time.sleep(5)
    #
    #         date_dropdown = {"id": "qa-dateDropDownOptions"}
    #         assert self.selenium_driver.get_element(date_dropdown)
    #         # Open Log Date drop down
    #         self.selenium_driver.get_element(date_dropdown).click()
    #         x = self.selenium_driver.get_element(date_dropdown)
    #         dropdown_values = x.find_elements_by_class_name("ui-dropdown-items-wrapper")
    #         time.sleep(3)
    #
    #         # convert month name to number
    #         # verify that a corresponding value is there in the drop down & button is clickable
    #         pass
    #     # self.check_unsaved_changes_popup()
    #
    #     download_log_button = {"id": "qa-generalSettingsDownloadButton"}
    #     assert self.selenium_driver.get_element(download_log_button)
    #     print(inspect.stack()[0][3] + ": Finished \n")
    #
    # def test_siem_settings(self):
    #     self.navigate_to_screen('SETTINGS')
    #     # Siem settings
    #     siem_settings_tab = {"id": "ui-tabpanel-1-label"}
    #     assert self.selenium_driver.get_element(siem_settings_tab)
    #     self.selenium_driver.perform_action('Click', self.selenium_driver.get_element(siem_settings_tab))
    #     siem_enabled = {"id": "siem_enabled"}
    #     self.selenium_driver.get_element(siem_enabled)
    #     # self.check_unsaved_changes_popup()
    #     print(inspect.stack()[0][3] + ": Finished \n")
    #
    # def test_active_directory(self):
    #     self.navigate_to_screen('SETTINGS')
    #     # Active directory Settings
    #     active_directory_tab = {"xpath": "//*[contains(text(), 'ACTIVE DIRECTORY')]"}
    #     self.selenium_driver.perform_action('Click', self.selenium_driver.get_element(active_directory_tab))
    #     # assert self.selenium_driver.get_element(active_directory_tab)
    #     # self.check_unsaved_changes_popup()
    #     enable_ad = {"xpath": "//*[contains(text(), 'Enabled Active Directory Connectivity')]"}
    #     assert self.selenium_driver.get_element(enable_ad)
    #     self.selenium_driver.perform_action('Click', self.selenium_driver.get_element(enable_ad))
    #     time.sleep(10)
    #     xx = self.selenium_driver.get_element(enable_ad).is_enabled()
    #     # self.check_unsaved_changes_popup()
    #     # print xx
    #     print(inspect.stack()[0][3] + ": Finished \n")
    #     pass
    #
    # def test_db_settings(self):
    #     self.navigate_to_screen('SETTINGS')
    #     # DB Settings #TODO need to find a way to click the DB tab in the bar
    #     db_settings_tab = {"xpath": "//*[contains(text(), 'DB')]"}
    #     assert self.selenium_driver.get_element(db_settings_tab)
    #     print(inspect.stack()[0][3] + ": Finished \n")
    #
    # def test_user_settings(self):
    #     self.navigate_to_screen('SETTINGS')
    #     # User Settings TODO
    #     user_tab = {"xpath": "//*[contains(text(), 'USER')]"}
    #     assert self.selenium_driver.get_element(user_tab)
    #     self.selenium_driver.perform_action('Click', self.selenium_driver.get_element(user_tab))
    #     add_new_user = {"id": "qa-addUserButton"}
    #     assert self.selenium_driver.get_element(add_new_user)
    #     self.check_unsaved_changes_popup()
    #     print(inspect.stack()[0][3] + ": Finished \n")
    #
    # def test_report_settings(self):
    #     self.navigate_to_screen('SETTINGS')
    #     # Reports Settings TODO
    #     reports_tab = {"xpath": "//*[contains(text(), 'REPORTS')]"}
    #     assert self.selenium_driver.get_element(reports_tab)
    #     self.selenium_driver.perform_action('Click', self.selenium_driver.get_element(reports_tab))
    #     attack_checkbox = {"id": "report_type_1"}
    #     # assert self.selenium_driver.get_element(attack_checkbox).is_displayed()
    #     inner_checkbox = {"xpath": "//*[contains(text(), 'Attack Report')]"}
    #     checkbox = self.selenium_driver.get_element(inner_checkbox)
    #     xx = checkbox.is_selected()
    #     # print xx
    #     self.selenium_driver.perform_action('Click', self.selenium_driver.get_element(inner_checkbox))
    #     xx = checkbox.is_selected()
    #     # print xx
    #     time.sleep(3)
    #
    #
    #     radiocbd = {"name": "reportTypeCb"}
    #     # assert self.selenium_driver.get_element(radiocbd).is_displayed()
    #     # assert checkbox.is_displayed()
    #     # self.check_unsaved_changes_popup()
    #     time.sleep(3)
    #     print(inspect.stack()[0][3] + ": Finished \n")
    #
    # def test_mail_settings(self):
    #     self.navigate_to_screen('SETTINGS')
    #     # Mail Settings TODO
    #     mail_settings_tab = {"xpath": "//*[contains(text(), 'MAIL')]"}
    #     assert self.selenium_driver.get_element(mail_settings_tab)
    #     self.selenium_driver.get_element(mail_settings_tab).click()
    #     # self.check_unsaved_changes_popup()
    #     print(" \n" +inspect.stack()[0][3] + ": Finished \n")
    #
    # def test_logout_action(self):
    #     user_menu = {"class": "action-menu-wrapper"}
    #     self.selenium_driver.get_element(user_menu)
    #     # Click on User Menu
    #     self.selenium_driver.perform_action('Click', self.selenium_driver.get_element(user_menu))
    #     user_actions_menu = {"xpath": "//*[@class='action-menu-item ng-star-inserted']"}
    #     sub_menu_items = self.selenium_driver.get_elements(user_actions_menu)
    #
    #     # Need to improve Logout button identification
    #     self.selenium_driver.perform_action('Click', sub_menu_items[1])
    #     self.test_ui_verify_login_screen()
    #     print(" \n" + inspect.stack()[0][3] + ": Finished \n")

    def teardown_class(self):
        time.sleep(5)
        self.selenium_driver.driver.close()
        pass

    # Aux methods
    def check_unsaved_changes_popup(self):
        # TODO make sure it works everytime , at the moment it's failing , possibly context dependant
        popup_menu = {"xpath": "//*[contains(text(), 'Leave this tab')]"}
        if self.selenium_driver.get_element(popup_menu):
            self.selenium_driver.perform_action('Click', self.selenium_driver.get_element(popup_menu))

    def navigate_to_screen(self, screen_title):
        """
        Navigates to the specified screen in the server UI
        :param screen_title:
        """
        attack_screen_redirect = {"xpath": "//*[@class='sidebar-item-text']"}
        sidebar_links = self.selenium_driver.get_elements(attack_screen_redirect)
        assert sidebar_links

        if len(screen_title) == 0:
            raise NotImplementedError
        elif screen_title == 'MAIN':
            sidebar_links["0"].click()

        elif screen_title == 'ATTACKS':
            # Navigate to attacks Screen
            attacks_distribution_label = {"class": "top-attacks-header"}
            self.selenium_driver.perform_action('Click', sidebar_links[int("1")], None, attacks_distribution_label)

        elif screen_title == 'PROTECTORS':
            # Navigate to Protectors Screen Screen
            self.selenium_driver.perform_action('Click', sidebar_links[2], None)

        elif screen_title == 'PLANS':
            # Navigate to Settings Screen
            self.selenium_driver.perform_action('Click', sidebar_links[3], None)

        elif screen_title == 'SETTINGS':
            # Navigate to Settings Screen
            self.selenium_driver.perform_action('Click', sidebar_links[4], None)

    def clear_login_fields(self):
        # Clear text fields:
        user_name = {"id": "username"}
        self.selenium_driver.perform_action('ClearText', self.selenium_driver.get_element(user_name))
        password = {"id": "password"}
        self.selenium_driver.perform_action('ClearText', self.selenium_driver.get_element(password))
        time.sleep(5)

    def get_top_5_data(self, elements_list):
        for single_tuple in elements_list:
            tuple_name = single_tuple.find_element_by_class_name("graph-list-item-title-text").get_attribute("title")
            tuple_count = int(single_tuple.find_element_by_class_name("graph-list-item-count-text")
                               .get_attribute("innerHTML"))
            # TODO store the tuple name and count in to a dictionary
            # TODO Add comparison/verification to expected values against the created dictionary
            # print tuple_name
            # print tuple_count
        pass
